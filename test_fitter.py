import matplotlib.pyplot as plt
import numpy as np
import cms_fstyle as plotting
from cms_fstyle.fitter import FFitter


def resolution(xx, *ppp):
    return np.sqrt(ppp[0]**2/xx**2 + ppp[1]**2/xx + ppp[2]**2)


if __name__ == '__main__':
    x = np.array([1.5, 2.5, 3.5, 4.5, 7.5, 15., 30., 50., 70., 90.])
    y1 = np.array([0.15051067, 0.12468743, 0.09814444, 0.07720865, 0.04943175, 0.02838496,
                   0.0163057, 0.011149, 0.00865279, 0.00828463])
    n1 = np.array([677, 1005, 975, 982, 5077, 9891, 20118, 19611, 18929, 18937])
    yerr1 = y1 / np.sqrt(n1)

    y2 = np.array([0.20159527, 0.17363882, 0.11680037, 0.11299323, 0.07562562, 0.04571766,
                   0.02736148, 0.01934412, 0.01520969, 0.01234431])
    n2 = np.array([941, 1018, 933, 982, 5082, 9924, 20308, 20012, 19783, 19765])
    yerr2 = y2 / np.sqrt(n2)

    fitter = FFitter(p0=(0.2, 0.05, 0.005), p_range=[[0, 0.5], [0, 0.5], [0, 0.1]])
    par1, cov1 = fitter.fit(x, y1, yerr=yerr1, func=resolution)
    xfun1, yfun1 = fitter.curve()
    par2, cov2 = fitter.fit(x, y2, yerr=yerr2, func=resolution)
    xfun2, yfun2 = fitter.curve()

    print(par1)
    print(par2)

    plotting.draw(x, y1, yerr=yerr1, option='E', legend='model')
    plt.plot(xfun1, yfun1, color=plotting.last_color(), ls='--')
    plotting.draw(x, y2, yerr=yerr2, option='E', legend='pf')
    plt.plot(xfun2, yfun2, color=plotting.last_color(), ls='--')

    plt.xscale('log')
    plotting.polish_axis(x_range=(1, 100), y_range=(0, 0.25),
                         x_title='E (GeV)', y_title='$\sigma_E / E$', leg_title='')

    plotting.show()
